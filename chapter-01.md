## Chapter 01 - History of version control system / Source control management:
* CVS - Concurrent Versions System. (1986 - 2008)
* SVN - Apache Subversion. (2000 - today)
* Git - (2005 - today)


### History of git:

During the early years of the Linux kernel maintenance (1991–2002), changes to the software were exchanged as patches and archived files/tar-balls. 

In 2002, the Linux kernel project began using a proprietary version control system called BitKeeper.

In 2005, BitKeeper's free-of-charge status was revoked. Linus Torvalds decided to develop his own tool, and called it "git".

Primary features of Git:
* Speed
* Simple design
* Strong support for non-linear development (thousands of parallel branches)
* Fully distributed
* Able to handle large projects efficiently. e.g. Linux kernel (in terms of speed and data size)


## Basic unit of work in git - the "commit":
* Briefly show tree structure and how commits point/references to the previous/parent commit.

## Other learning resources for Git:
* [The git book](https://git-scm.com/book/en/v2)
