## Chapter 7 - Add collaborators: (collaborate on single / master branch)
* Show picture of a plant with single stem and leaves on that stem.
* Gitlab "Groups", "Projects" and "members"
* members "clone" the existing repo to their computers / home directories
* Use gitlab "Issues" and "Kanban" board for communication, project management and visibility
* Use priority and size labels
* each developer creates new files, commits and pushes them to remote/origin
* Show these commits on a paper / graph. Show each developer's local tree.
* Above will result in different errors, depending on situation:
 * merge conflicts, 
 * unable to push, 
 * unable to pull
* Learn to resolve the errors
* Use stash area to temporarily store your uncommitted work.
* Lesson learned: This model is difficult to use, because each time the world state changes, you will need to update your local copy, resulting in conflicts.
* Types of commits:
 * Commits created by you
 * Commits created by other team members
 * Commits made by git (merge commits)
