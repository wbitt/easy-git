# Chapter 02 - Git setup on Local computer, and Gitlab:

## Install git on local work computer:
If you already have git installed on your computer, then verify the version of git. 

```
git version
```

If git is not installed, or is not the latest, installing a latest version is useful.

**Linux (RedHat & derivatives):**
```
yum -y install git
```

**Linux (Debian & derivatives):**
```
apt-get update
apt-get -y install git
```

**macOS:**
```
brew install git
```

**Windows:**
Download and run the installer from: [https://git-scm.com/download/win](https://git-scm.com/download/win)

For windows users, it is important that during the git's installation process, you choose:
* Use OpenSSL library
* Use MinTTY (the default terminal of MSYS2)
* `git pull` behavior: fast-forward of merge - which is the default
* Git Credential Manager core
* Enable file system caching
* **"Checkout Windows-style, commit Unix-style line endings"** - to save yourself from lot of trouble. This sets up correct CR/LF (aka CRLF) (Carriage Return / Line Feed) settings for line endings in your text files. To control this yourself, you can set the following settings. This will make sure when you do a checkout in windows, all `LF` will convert to `CRLF`, and when you commit your changes back into repository, all `CRLF` will be converted back to `LF`.

| ![Git installer on Windows - CRLF](images/chapter-02_windows-crlf.png) |
|------------------------------------------------------------------------|


## Configure git:
Setup / configure git's global settigs (user,email, default branch-name, editor, CR/LF, log alias, etc) on local computer

**Note:** Using Visual Studio Code on slower computers is a nightmare. If you have a computer with low resources, try using lightweight editors: vi, geany, notepad, notepad++.

### Basic configuration:

```
git config --global user.name "Kamran Azeem"
git config --global user.email kamran@wbitt.com
```

By default `git init` will create a branch named `master`. If you want a different name, e.g.  `release-train`, `development`, `main` or `trunk`, you can do it in git version 2.28+ .
```
git config --global init.defaultBranch master
```



Windows settings for CR/LF:
```
git config --global core.autocrlf true
```

Linux & macOS settings for CR/LF:
No explicit setting required, as the default is to use only `LF` as line endings.


Setup "aliases" for commonly used commands:
```
git config --global alias.lg 'log --oneline --graph --all --decorate'
```

Summary of `git log` options shown in the alias above:
* `--oneline`
> <hash>  <title line>
> This is a shorthand for "--pretty=oneline --abbrev-commit" used together with `git log`, and is designed to be as compact as possible. 

* `--graph`
> Draw a text-based graphical representation of the commit history on the left hand side of the output. This may cause extra lines to be printed in between commits, in order for the graph history to be drawn properly. This implies the `--topo-order` option by default, but the `--date-order` option may also be specified.

* `--all`
> By default, git log will only show commit history below the branch you’ve checked out. To show logs from all branches, use `--all` with `git log` command.

* `--decorate`
> Print out the ref names of any commits that are shown.


### Configure default editor for git's use:
This is used to write commit messages, or commit messages while merging, or commit messages while resolving conflicts. Default is **"vim"** which I consider **"the best"**, but you can change it if you want, as shown below. On windows, you can choose an editor of your choice during git's installation process. It is expected that the editor you choose is present/installed on your computer. Notepad happens to be on availble out of the box on all Windows computers, and is a sane choice.

My advise is not to be tempted by the advanced IDEs, and just use simplest/lightest possible editors. i.e. One of: "vim", "notepad" or "notepad++" only.
 
```
git config --global core.editor "vim"
```

```
git config --global core.editor "notepad"
```

```
git config --global core.editor "'C:/Program Files/Notepad++/notepad++.exe' -multiInst -notabbar -nosession -noPlugin"
```

```
git config --global core.editor "atom --wait"
```

```
git config --global core.editor "code --wait"
```

```
git config --global core.editor "subl -n -w"
```

## Display current git configuration:

### Configuration from a Linux system: 
```
[kamran@kworkhorse ~]$ git config --list --show-origin
file:/home/kamran/.gitconfig    user.email=kamranazeem@gmail.com
file:/home/kamran/.gitconfig    user.name=Kamran Azeem
file:/home/kamran/.gitconfig    credential.helper=store
file:/home/kamran/.gitconfig    alias.lg=log --oneline --graph --decorate --all
file:/home/kamran/.gitconfig    push.default=simple
file:/home/kamran/.gitconfig    push.followtags=true
file:/home/kamran/.gitconfig    branch.autosetuprebase=always
file:/home/kamran/.gitconfig    github.user=kamranazeem
[kamran@kworkhorse ~]$ 
```

### Configuration from a Windows computer:
```
DemoUser@DemoPC MINGW64 ~
$ git config --list --show-origin
file:C:/Program Files/Git/etc/gitconfig diff.astextplain.textconv=astextplain
file:C:/Program Files/Git/etc/gitconfig filter.lfs.clean=git-lfs clean -- %f
file:C:/Program Files/Git/etc/gitconfig filter.lfs.smudge=git-lfs smudge -- %f
file:C:/Program Files/Git/etc/gitconfig filter.lfs.process=git-lfs filter-process
file:C:/Program Files/Git/etc/gitconfig filter.lfs.required=true
file:C:/Program Files/Git/etc/gitconfig http.sslbackend=openssl
file:C:/Program Files/Git/etc/gitconfig http.sslcainfo=C:/Program Files/Git/mingw64/ssl/certs/ca-bundle.crt
file:C:/Program Files/Git/etc/gitconfig core.autocrlf=true
file:C:/Program Files/Git/etc/gitconfig core.fscache=true
file:C:/Program Files/Git/etc/gitconfig core.symlinks=false
file:C:/Program Files/Git/etc/gitconfig pull.rebase=false
file:C:/Program Files/Git/etc/gitconfig credential.helper=manager-core
file:C:/Program Files/Git/etc/gitconfig credential.https://dev.azure.com.usehttppath=true
file:C:/Program Files/Git/etc/gitconfig init.defaultbranch=master
file:C:/Users/DemoUser/.gitconfig       user.email=demouser@example.com
file:C:/Users/DemoUser/.gitconfig       user.name=Demo User
file:C:/Users/DemoUser/.gitconfig       core.editor=notepad
file:C:/Users/DemoUser/.gitconfig       alias.lg=log --oneline --graph --decorate --all

DemoUser@DemoPC MINGW64 ~
$
```

## Scope of Git configuration files:
The configuration shown above from a Linux and Windows computer is pulled from the System and Global configuration files. There are three levels/scopes for most of configurations, where configuration found in a lower level overrides configuration from a previous/higher level.

* System level - `/etc/gitconfig` (Linux) or `C:/Program Files/Git/etc/gitconfig` (Windows)
* Global level - `~/.gitconfig`
* Repository level - `<path-of-repository-on-disk>/.git/config`

One example would be that your `user.name` and `user.email` is different at each level, such as:
* System level:
  * `user.name=Kamran Azeem`
  * `user.email=kamran@wbitt.com`

* Global level: (mandatory - various default settings for all git repositories on your computer)
  * `user.name=Kamran Azeem`
  * `user.email=kamran@praqma.com`

* Repository level: (optional - use on a repository with special needs)
  * `user.name=Demo User`
  * `user.email=demouser@example.com`

So if you create a repository on your local computer, and try to create a commit, git would want to know what is the identity of this user (name and email). It will try to pick this information from Repository level/scopt, i.e. `<path-of-repository-on-disk>/.git/config`. If it does not find this information there, then it will try in the Global level/scope, i,e. `~/.gitconfig`. If it does not find it there ,(which is not likely, but still), then it will try to find this information in the System level/scope, i.e. `/etc/gitconfig` on Linux, and `C:/Program Files/Git/etc/gitconfig` on Windows.

**Note:** There is no particular use of setting up `user.name` and `user.email` in the System level/scope. The above example is merely for understanding. 


## Setup on Gitlab (or any other git provider):

Now is the time to setup your account on a git service provider of your choice. For this book (and training course), I have setup my account on [https://gitlab.com](https://gitlab.com) and secured it with 2FA (Two Factor Authentication). If you already have an account at GitLab, then simply login, if you don't, you can create a free account. 

You can use external authentication systems such as Google or GitHub to create your account on GitLab. Just ensure that your google account is also secure by 2FA. 

It goes without saying that all your accounts on various websites on the internet (including social media) must be secured by 2FA, for your (and your family's) security and safety.

Using 2FA is important because:
* (a) it acts as a very good defense against people trying to misuse (or hack into) your gitlab account by simply using your username and password,
* (b) many organizations and individuals (like me) require team members to have their account secured by 2FA before they are allowed to access any of the repositories. Below is an example of my gitlab group named **"wbitt"**, which requires all users in my this group to setup 2FA.


| ![images/chapter-02_enforce-2fa.png](images/chapter-02_enforce-2fa.png) |
|-------------------------------------------------------------------------|

There are several mechanisms available to enable 2FA, including - but not limited to: SMS, authentication app on your mobile phone such as Google Authenticator, Authy, etc.

Just so you know, there are three factors of authentication:
* Something you know - e.g. passwords
* Something you have - e.g. key, token, authentication app, etc
* Something you are  - e.g. fingerprint, retina scan, face scan, etc

Using any one of them at any given time is just single factor authentication (or 1FA), which is not secure enough. Therefore you should use a combination of at least two of them making it 2FA - or, ideally - all of them, making it 3FA! . Most of the services on the internet at the moment provide a maximum of 2FA level of authentication security.

For example, to log on to Gitlab, I am using Gitlab Username and Password, and Google Authenticator app on my mobile phone. My phone in-turn is protected by a screen lock with a number of more than ten unique digits! :) I don't trust bio-metric devices for security and privacy concerns. So this is my 2FA setup.

In gitlab's web UI, you can enable Two-Factor Authentication by clicking your account name / profile picture on the top right of the page, and then select `Edit Profile -> Account` 
 
| ![images/chapter-02_profile-2fa.png](images/chapter-02_profile-2fa.png) |
|-------------------------------------------------------------------------|

### SSH keypair:

From the point you enable 2FA on your gitlab profile, you won't be able to use the git clone/fetch/pull/push commands on HTTPS based repository URLs/addresses using your gitlab username and password. The `git` command line utility has no way to take in a third input for your 2FA while working with HTTPS. This means, you will not be able to clone/fetch/pull/push on any private repository, nor on any of your own private and public repositories. The only operation allowed would be the ability to clone public / open-source repositories using the HTTPS URL.

None of them is a problem, and it should not annoy you at all. These are very important security measures. So the question is what to do? 

The answer is: you create a **passphrase-protected openssh keypair**. So the **key** would be **"Something you have"**, and the **passphrase** would be **"Something you know"**, making it a 2FA device. Then, upload the public part of the key to your gitlab profile under "SSH Keys". Once done, gitlab will let you do all operations on all sorts of repositories using your openssh keypair. It goes without saying that passphrase should be easy to remember for you, but difficult to guess for others. It should ideally be longer than eight characters, and should use a mix of alphanumeric characters, and special characters.

**Note:** The private key is for you to keep secret, so never publish the private key/identity file anywhere, nor give it to anyone. The passphrase protects the key *in-case* it ever gets leaked/lost. A SSH key without a passphrase is completely useless.

It is important that you create this SSH keypair on a Linux or macOS computer, or in **git bash** on Windows computers, because this keypair is required to be in openssh format. *By default* "PuttyGen" (on Windows) does not create SSH keypair in openssh format. You need to select certain options manually to be able to do that. Since you do get git bash once you install git on Windows, it is best to use `ssh-keygen` command available in git bash.


Here is how you create a SSH keypair using ED25519 encryption, which is securer and more efficient compared to RSA:

```
[kamran@kworkhorse ~]$ ssh-keygen -t ed25519
Generating public/private ed25519 key pair.
Enter file in which to save the key (/home/kamran/.ssh/id_ed25519): 
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/kamran/.ssh/id_ed25519
Your public key has been saved in /home/kamran/.ssh/id_ed25519.pub
The key fingerprint is:
SHA256:Yd4uHLNKOFjIPcXD5WHl47KCttGWcX1Ci5P7FRcu0UU kamran@kworkhorse.oslo.praqma.com
The key's randomart image is:
+--[ED25519 256]--+
|        +..    .E|
|     o + o   . . |
|      = + + . o  |
| . o . + O o o . |
|  o + . S * + o  |
|   o = = X o +   |
|  . * * * . .    |
|   . * o o .     |
|    . .   .      |
+----[SHA256]-----+
[kamran@kworkhorse ~]$ 
```

Now, display the public part of your SSH keypair, and then upload it on gitlab under your profile. 

**Note:** Don't worry, displaying, publishing, sharing your public key is very safe, and is the intended use of the public key!

```
[kamran@kworkhorse ~]$ cat ~/.ssh/id_ed25519.pub 
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDROn5fovT1lZnJWPu3/cWuSjUyxh5pjme3JMvuWcdZg Kamran-ed25519-key
[kamran@kworkhorse ~]$ 
```

| ![images/chapter-02_profile-ssh-key.png](images/chapter-02_profile-ssh-key.png) |
|---------------------------------------------------------------------------------|



Some people create different sets of SSH keypairs for different uses, and/or create them at different location on their computer instead of the default location ( `~/.ssh/id_ed25519` and `~/.ssh/id_ed25519.pub` ) . In either case, you will need to inform SSH that each time it tries to connect to a remote ssh-aware service, it should load it's private key (aka identity file) from that location. 

You edit the `~/.ssh/config` file for your OS user, and specify the exact path and exact name of the private key / identity file.
```
# vi ~/.ssh/config
Host gitlab.com
    Hostname gitlab.com
    User git
    # IdentityFile ~/.ssh/id_ed25519
    IdentityFile ~/Keys-Passwords/kamran-id_ed25519
```

**Note:** Since the SSH keypair is protected by a passphrase, each time you do a git clone/fetch/pull/push, the command line will ask you for the passphrase. On Linux systems it is not a problem, as the GUI keeps it in a system keyring the moment the key is used the first time and never asks you again, until the GUI session expires, user logs out or the system restarts. On Windows I am not aware of any mechanisms which keep the key in some keyring - as I don't use Windows at all. This means you will be required to type in the passphrase quite frequently!
