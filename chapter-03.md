# Chapter 3 - Basic operations/Basic tasks:
In chapter 1, we learned how git is structured. That was brief introduction and was intentionally so. My plan is to actually show how it works in real life, with real file and directories, so you can develop a clear visual mental model of it.

In this chapter, we will create a simple repository, and do certain basic git operations on it. This repository will obviously be created in a directory on local computer. It will contain code to serve a very simple static HTML website. 

## Directory and repository setup:

Here is what the initial layout of the directory:
```
./simple-website
├── aboutus.html
├── contactus.html
├── css
│   └── main.css
├── index.html
├── js
│   └── main.js
└── products.html
```

Lets create this directory, and create the desired structure in it. It is important to have a well thought out directory setup for organization of your files on the OS filesystem. You don't want your work to be all over the place. 

Before I create the above mentioned directory, I want to show how I arrange all my git repositories. 

My main **Projects** directory is: `/home/kamran/Projects/`. Inside it I have sub-directories for different people I work with, including my employer:


```
[kamran@kworkhorse Projects]$ pwd
/home/kamran/Projects
```

```
[kamran@kworkhorse Projects]$ tree -d -L 1
.
├── AzharSb
├── FeysalSb
├── IngvarSb
├── MurtazaSb
├── Personal
├── Praqma
└── WaqarSb

7 directories
[kamran@kworkhorse Projects]$ 
```

Under `Personal` , I have a lot of directories, but I will show some relevant ones:

```
[kamran@kworkhorse Personal]$ pwd
/home/kamran/Projects/Personal
```

```
[kamran@kworkhorse Personal]$ tree -d -L 1
.
├── docker-lamp
├── DockerTalk-OSFP-2015
├── eicar-virus-files
├── github
├── gitlab
├── kamran-websites
├── LibVirt
└── qmail

.. directories
[kamran@kworkhorse Personal]$ 
```

Inside gitlab, I have all my projects hosted on gitlab. 

**Note:** You should not get confuse with the word "project". It is simply a piece of work, which may or may not be a git repository. It may be a collection of some related git repositories stored under one name. This will become clearer later.


```
[kamran@kworkhorse gitlab]$ pwd
/home/kamran/Projects/Personal/gitlab
```

```
[kamran@kworkhorse gitlab]$ tree -d -L 1
.
├── development-on-shared-mac
├── docker-simple-website
├── easy-git
├── geany-setup
├── gitlab-ci-demo
├── home-studio
├── Learn-Angular
├── learn-istio
├── learn-javascript
├── learn-php
├── learn-php-salman-sb
├── monitor-and-alert
├── public-ssh-keys
├── radionicsclinic.com
├── simpleapp.demo.wbitt.com
├── ssh-server
└── wbitt
```

Lets create our simple-website directory here and create necessary structure inside it.

```
[kamran@kworkhorse gitlab]$ pwd
/home/kamran/Projects/Personal/gitlab
```

```
mkdir simple-website
cd simple-website
mkdir css js
echo "About us" > aboutus.html 
echo "Contact us" > contactus.html 
echo "Simple HTML static website" > index.html 
echo "Our products" > products.html
echo "// Main styling file" > css/main.css
echo "// Main javascript file" > js/main.js
```

Here is how it looks like:

```
[kamran@kworkhorse simple-website]$ tree .
.
├── aboutus.html
├── contactus.html
├── css
│   └── main.css
├── index.html
├── js
│   └── main.js
└── products.html


2 directories, 6 files
[kamran@kworkhorse simple-website]$
```

```
[kamran@kworkhorse simple-website]$ ls -lha
total 32K
drwxrwxr-x  4 kamran kamran 4.0K Jun 28 15:45 .
drwxrwxr-x 32 kamran kamran 4.0K Jun 28 16:05 ..
-rw-rw-r--  1 kamran kamran    9 Jun 28 16:14 aboutus.html
-rw-rw-r--  1 kamran kamran   11 Jun 28 16:15 contactus.html
drwxrwxr-x  2 kamran kamran 4.0K Jun 28 15:44 css
-rw-rw-r--  1 kamran kamran   27 Jun 28 16:15 index.html
drwxrwxr-x  2 kamran kamran 4.0K Jun 28 15:44 js
-rw-rw-r--  1 kamran kamran   13 Jun 28 16:15 products.html
[kamran@kworkhorse simple-website]$ 
```

Creating a directory and/or filling it up with files, etc, does not automatically make it a git repository. You have to *initialize* it as a git repository. 

To check if a directory is git repository or not, do a `git status` in it. If it gives you an error, it is not a git repository. If the directory does not contain a special `.git` subdirectory in it, then that is also a tell-tale sign that it is not a git repository. The directory listing shown above does not contain a `.git` subdirectory.

```
[kamran@kworkhorse simple-website]$ git status
fatal: not a git repository (or any parent up to mount point /)
Stopping at filesystem boundary (GIT_DISCOVERY_ACROSS_FILESYSTEM not set).
[kamran@kworkhorse simple-website]$
```

Lets initialize this directory as a git repository:

```
[kamran@kworkhorse simple-website]$ git init
Initialized empty Git repository in /home/kamran/Projects/Personal/gitlab/simple-website/.git/
[kamran@kworkhorse simple-website]$ 
``` 

Don't be worried about the word "empty" above. Your files have not suddenly disappeared. It is simply saying that it did not find a `.git` directory, and therefore it created `.git` sub-directory, essentially making this directory a **git repository**. The "repository" is "empty" at the moment, as you have not checked in any code in it yet.

```
[kamran@kworkhorse simple-website]$ ls -lha
total 36K
drwxrwxr-x  5 kamran kamran 4.0K Jun 28 16:19 .
drwxrwxr-x 32 kamran kamran 4.0K Jun 28 16:05 ..
-rw-rw-r--  1 kamran kamran    9 Jun 28 16:17 aboutus.html
-rw-rw-r--  1 kamran kamran   11 Jun 28 16:15 contactus.html
drwxrwxr-x  2 kamran kamran 4.0K Jun 28 15:44 css
drwxrwxr-x  7 kamran kamran 4.0K Jun 28 16:19 .git
-rw-rw-r--  1 kamran kamran   27 Jun 28 16:15 index.html
drwxrwxr-x  2 kamran kamran 4.0K Jun 28 15:44 js
-rw-rw-r--  1 kamran kamran   13 Jun 28 16:15 products.html
[kamran@kworkhorse simple-website]$
```

Please note that the name of this directory is `.git` instead of `git`, and has a clever purpose. On Unix/Linux, the leading "dot" makes an object hidden in the default directory listing, such as `ls` or `ls -l` , unless you use a special switch `-a` to show hidden files too. So naming it `.git` keeps it hidden from you during general file operations, and you are not bothered with it. 

Also, git is designed to always ignore this directory while doing any git operations. This is the git database so to speak. All the different versions of your various files are all stored inside it. So be careful, if you accidentally delete it, you will lose all your changes stored locally. We will talk about avoiding this accident later in this chapter by using a remote location to keep a copy of your local work.

Right, so if you do a `git status` now, you will see interesting information:

```
[kamran@kworkhorse simple-website]$ git status
On branch master

No commits yet

Untracked files:
  (use "git add <file>..." to include in what will be committed)
	aboutus.html
	contactus.html
	css/
	index.html
	js/
	products.html


nothing added to commit but untracked files present (use "git add" to track)
[kamran@kworkhorse simple-website]$ 
```

This requires explanation. So, the directory already existed before and we executed the `git init` command. After the directory is initialized as a git repository, git creates the first branch and calls it **master**. It also sees that the repository itself is empty ("No commits yet"), though it finds some files in it's **"working directory"** - or **"working tree"** - which are not being tracked by git, and therefore *can* be added to this repository. 

## Your first commit:

So now you have a git repository, nothing in it yet, but that it ok. The first thing we will do is to add *all* of the un-tracked files from the working directory to the local git repository. From this point on, things will start becoming interesting.

```
[kamran@kworkhorse simple-website]$ git add .
```

Check the status again. This time, the message has changed. The files which were un-tracked before in the working directory are now "Ready to be committed", and are in a special git area called **"Staging"**. Git simply considers them "changes" - thus the message "Changes to be committed". 

If adding files was not an intended operation, git tells you that you can "unstage" the files by using the `git rm --cached <file>` to "unstage". Since we actually intended the add operation, we will ignore the "unstage" tip.
  
```
[kamran@kworkhorse simple-website]$ git status
On branch master

No commits yet

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)
	new file:   aboutus.html
	new file:   contactus.html
	new file:   css/main.css
	new file:   index.html
	new file:   js/main.js
	new file:   products.html

[kamran@kworkhorse simple-website]$ 
```

So, are the files part of the repository at this point? No! It already says "No commits yet" in the second line in the output above. If you run `git log` now, you will get a similar message.

```
[kamran@kworkhorse simple-website]$ git log 
fatal: your current branch 'master' does not have any commits yet
[kamran@kworkhorse simple-website]$
```

So, the files we added are just sitting in the "staging" area. To be able to actually commit them in the repository, you run the `git commit`.

```
[kamran@kworkhorse simple-website]$ git commit -m "Initial commit."
[master (root-commit) b7a91bb] Initial commit.
 6 files changed, 18 insertions(+)
 create mode 100644 aboutus.html
 create mode 100644 contactus.html
 create mode 100644 css/main.css
 create mode 100644 index.html
 create mode 100644 js/main.js
 create mode 100644 products.html

[kamran@kworkhorse simple-website]$
``` 

Ok. Lets understand the output. This was our first commit on the master branch in the repository, so the first line says "root-commit" i.e. a commit with no parent. The number `97a91bb` is the short form of the SHA hash of the commit. The rest of the lines simply state the obvious.

If you do a git status now, you will see that the working directory/tree is clean.

**Note:** I will use terms "working directory" and "working tree" interchangeably throughout this book.
```
[kamran@kworkhorse simple-website]$ git status
On branch master
nothing to commit, working tree clean
[kamran@kworkhorse simple-website]$ 
```

If you run `git log` now, you will see your first commit:

```
[kamran@kworkhorse simple-website]$ git log
commit b7a91bbf193de0a8d5988deafd74a56cde9181c2 (HEAD -> master)
Author: Kamran Azeem <kamranazeem@gmail.com>
Date:   Tue Jun 29 22:08:43 2021 +0200

    Initial commit.
[kamran@kworkhorse simple-website]$ 
```

This also deserves some explanation. The first line shows a **"commit ID"** or **"commit hash"**, which is a SHA hash. Normally you will see the long hash only in the git log output. The shorter form (first seven digits of the hash), is something you will see more often. The same line says **"HEAD->master"**, which means that HEAD points to - or, is on - the **"master"** branch. This line also means that the HEAD, and the master branch are both on the commit "97a91bb". (Note: "master branch" is actually just a pointer, which will move with commits. You will see this shortly).

Also, the Author Name and email were configured earlier in the previous chapter, and git simply uses it here, and makes it part of the commit. Then is of-course the timestamp at which the commit was saved in the local repository. The last line is the commit message which you passed to `git commit` command with a `-m` switch.

If you want a shorter output, you can use another form of `git log` command, shown below. We already setup an alias for it in previous chapter.

```
[kamran@kworkhorse simple-website]$ git  log --oneline --graph --all --decorate
* b7a91bb (HEAD -> master) Initial commit.
[kamran@kworkhorse simple-website]$ 
```

Or:

```
[kamran@kworkhorse simple-website]$ git lg
* b7a91bb (HEAD -> master) Initial commit.
[kamran@kworkhorse simple-website]$ 
```

**Tip:** If you don't have the alias setup, or simply want to remember the full command in your head, the easier way is to remember the initial letters of all the switches. I remember it as `git logad` , meaning: "Log, Oneline, Graph, All, Decorated". :)

Right, you can also use `git show <commit-id>` command to show what does the commit look like:

```
[kamran@kworkhorse simple-website]$ git show b7a91bb
commit b7a91bbf193de0a8d5988deafd74a56cde9181c2 (HEAD -> master)
Author: Kamran Azeem <kamranazeem@gmail.com>
Date:   Tue Jun 29 22:08:43 2021 +0200

    Initial commit.

diff --git a/aboutus.html b/aboutus.html
new file mode 100644
index 0000000..06cc739
--- /dev/null
+++ b/aboutus.html
@@ -0,0 +1 @@
+About us
diff --git a/contactus.html b/contactus.html
new file mode 100644
index 0000000..428c736
--- /dev/null
+++ b/contactus.html
@@ -0,0 +1 @@
+Contact us
diff --git a/index.html b/index.html
new file mode 100644
index 0000000..cef62fa
--- /dev/null
+++ b/index.html
@@ -0,0 +1 @@
+Simple HTML static website
diff --git a/products.html b/products.html
new file mode 100644
index 0000000..40817b7
--- /dev/null
+++ b/products.html
@@ -0,0 +1 @@
+Our products
diff --git a/js/main.js b/js/main.js
new file mode 100644
index 0000000..e6d207e
--- /dev/null
+++ b/js/main.js
@@ -0,0 +1 @@
+// Main javascript file
diff --git a/css/main.css b/css/main.css
new file mode 100644
index 0000000..086cac3
--- /dev/null
+++ b/css/main.css
@@ -0,0 +1 @@
+// Main styling file
[kamran@kworkhorse simple-website]$ 
```
 
Or:

```
[kamran@kworkhorse simple-website]$ git show b7a91bb --shortstat
commit b7a91bbf193de0a8d5988deafd74a56cde9181c2 (HEAD -> master)
Author: Kamran Azeem <kamranazeem@gmail.com>
Date:   Tue Jun 29 22:08:43 2021 +0200

    Initial commit.

 6 files changed, 6 insertions(+)
[kamran@kworkhorse simple-website]$ 
```

Okay, at this point, you have your first commit in the repository , and you have verified it with `git status`, `git log` and `git show` commands. Congratulations!

## Various git areas:

| ![Git Areas](images/chapter-03_git-areas.png) |
|-----------------------------------------------|


In this chapter, we will see 


* Create, or, clone a repository
* Add, commit changes (not push yet)
* Show status, log --all, commits, git show <commit>
* Explain various git areas (Working Directory (+ Stash), Staging Area / Index, Local repository (Objects), Remote / Origin)
* Show how file changes it's state when it moves from one area to another
* commit changes in a file multiple times, show what happens in each commit
* git mv
* discuss HEAD, origin/master
* modify files, and use `git diff` to show difference between working directory and local repo.
  * git diff ( compares what is in your working directory with what is in your staging area)
  * git diff --staged (compares your staged changes to your last commit in the repo)
  * git diff <commit> <commit>
* git commit --amend to amend last commit only.
* save work to remote by:
 * create a new repo on gitlab (private / public)? README?
 * add remote as origin
 * push commits to remote

* delete a file from local directory
* delete a file from repository (local and remote) - (advance topic)

* do couple of more commits on local repo
 * discuss HEAD and remote/HEAD origin/HEAD
 * ,and push them to remote

* git pull to pull latest changes from remote
* remove a unnecessary file from local repo, push commit to remote (git rm, versus OS rm)
* make sure you don't commit sensitive files, because a file is always in history no matter you delete it. And VERY PAINFUL process to get rid of it.
* Add .gitignore to prevent accidents in future
* git log --oneline --graph --all --decorate (set  as alias in your ~/.gitconfig)
* modify file, add and commit locally
* Show simple git diff, and git diff  between local repo and remote repo. (git diff --cached)
* Show file from previous commit
* checkout a file from previous commit
* Discard changes to a local file by "git restore" from staging area or from the local repo
* checkout a previous commit to look around in the code -> results in detached head. Resolve the problem.

* change git remote URL 
* What does git add and git commit do (internally):
  Git  stores blobs for the files that have changed, updates the index, writes out trees, and writes commit objects that reference the top-level trees and the commits that came immediately before them. These three main Git objects — the blob, the tree, and the commit — are initially stored as separate files in your .git/objects directory. 
