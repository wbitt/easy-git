# Learn git with example:


## Introduction:
`GIT`, or `git`, has been around for more than fifteen years now, but still many people around the world (students, developers, system administrators, etc), find it confusing and elusive. I want to admit that it was very elusive and confusing for me too for many years, and I needed help for anything beyond a simple git clone/add/commit/push. A git conflict used to make me sweat, and "detached head" was just plain nightmare. I attended several in-house courses, and a couple on various learning management websites, and nothing seem to clear my confusion. It remained as elusive as ever.

So, I eventually decided to utilize few of my free days, and bring this under control. I realized that most of the courses are not designed to help people understand the concept of git as a whole - and **how** is it useful for anyone working with it?; or, may be **why** is a more appropriate question. So after getting git under my control (almost), I created a scenario-based course-outline to help one of my friends and his team members. The session went very well, and the response was satisfying. So I went on and taught the same course to another friend and his team member, and their feedback was also satisfying. This made me think that not only this type of training course would be useful, a book covering the same topics would also be useful - thus this book!

I realized that people generally do not understand the internal workings of git right in the beginning of the course. So talking *too much* about commits, and their references back to parent commits, tres, and how these objects are stored is not very helpful in the beginning. Most people understand files and directories. So a more visual route around the files and directories is more likely to help understand the basic concepts. 

Most of the people normally hear about git through - or in relation to - GitHub or GitLab - or any of similar git providers. For those people, that (`git<fill in provider name here>`) **is git** - which is not true; but this is how it is. I think there is more chance of understanding how git works, if I could actually show how you create a repository, how to simply add files, commit and push, and how the work shows up in GitHub, GitLab, etc. It would even be more useful, if I could somehow show the work being used in a target environment, such as a simple static website being served through a web server! 

This proved to be very helpful way of learning (and teaching - of-course). I also decided to add necessary web interface features early in the course such as Issues, Labels, Kanban boards, etc; because that is what people are used to seeing - or at least used to hear these terms when they talk to their friends/colleagues. It also gives me a chance to teach Agile in a discreet way early on, and integrate it in their work-flow, while still keeping it fun, and easy to understand.

With that said, below is the scenario I came up with, which I will use in this course (and book) to teach git.

## My training course scenario:
I want to create/build a simple static HTML website. Initially, I will be alone on this project, but later, I want to involve a couple of my friends to help me improve it. I want to use git for managing all aspects of it.

My basic needs are:
* Ability to Version control my software (git itself)
* Ability to ensure integrity of software (checksum for each file and directory - SHA-1 hash)
* Ability to save work off-site - away from development computer (gitlab, github, etc)
* Ability to undo mistakes, etc (git revert, git reset)
* Ability to manage project, collaborate, communicate with others (gitlab, issues, boards, etc)
* Ability to test ideas, etc, without affecting the main code-base (git branches)
* Ability to mark certain points in time in the code base with special identification markers (git tags) 
* Later - ability to deploy automatically (CI/CD) 
* Later - ability to test automatically (CI/CD)
* Later - ability to host it free on Gitlab (CI/CD + Gitlab Pages)


# Table of Contents

* Section 1: How to use git? and how to use gitlab?
  * [Chapter 1 - History of version control system / Source control management](chapter-01.md)
  * [Chapter 2 - Git setup on Local computer, and Gitlab](chapter-02.md)
  * [Chapter 3 - Basic operations/Basic tasks](chapter-03.md)
  * [Chapter 4 - Undo changes](chapter-04.md)
  * [Chapter 5 - Git tags](chapter-05.md) 
  * [Chapter 6 - Trust models](chapter-06.md)
  * [Chapter 7 - Add collaborators (collaborate on single / master branch)](chapter-07.md)
  * [Chapter 8 - Pull vs Fetch](chapter-08.md)
  * [Chapter 9 - Local branches](chapter-09.md)
  * [Chapter 10 - Remote branches](chapter-10.md)
  * [Chapter 11 - Branch based development](chapter-11.md)
  * [Chapter 12 - Fast Forward vs 3-way merge](chapter-12.md)
  * [Chapter 13 - Pull/Merge requests](chapter-13.md)
  * [Chapter 14 - Repository Security / access](chapter-14.md)
  
  * [Chapter 15 - Other tools / tricks: Search, Bisect, etc](chapter-15.md)
  * [Chapter 16 - Advance Topic - delete sensitive file from repository / history](chapter-16.md)

Todo Using gitlab CLI

* Section 2: CI/CD with Gitlab
  * [Chapter 17 - Share sensitive information through gitlab CI environment variables](chapter-17.md)
  * [Chapter 18 - Continuous Deployment using git hooks](chapter-18.md)
  * [Chapter 19 - Continuous Deployment using Gitlab CI](chapter-19.md)
  * [Chapter 20 - Continuous Integration](chapter-20.md)
  * [Chapter 21 - Project: A complete automated website running on "Gitlab pages"](chapter-21.md)

 
