## Chapter 10 - Remote branches:
* Create a local branch
* Modify stuff and then commit changes in this new local branch
* Make this branch available on remote / master
* delete local branch and delete remote branch
* Discuss advantages and disadvantages of remote branches
