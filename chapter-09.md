## Chapter 9 - Local branches:
* Show picture of a plant or tree with multiple branches.
* A commit can have multiple children, or, multiple commits can have single parents; thus a branch.
* Benefits: 
 * ideally prevents team members from stepping onto each other's toes. Works best with a collective communication system such as Issues and Kanban board.
 * the world can keep moving forward while you can test your changes / ideas in isolated branches.
 * keeps the history on master branch clean and linear (using squash commits), which tells a better story
 * Easy to share ideas with your team members by sharing your local/test/feature branch as a remote branch
 
* Create branch 
* Make changes in files, commit them in this new branches
* switch to master again (git checkout), git pull to bring in new changes made by the rest of the world
* switch to your branch again
* merge master into your branch - to merge the work of others into your local/test branch while you continue working on it.
* when you finish your work, merge your branch into master, and push changes to remote.
* check your local branch is not on remote / origin
* delete local branch
* "We don't /like/want/do long lived branches"

* Git squash commit to squash multiple commits into a single one to keep master branch clean.

* Difference between merging a branch into master, and merging master into a branch


```
git branch -a

git checkout -b <branchname>

<do your work related to the issue>

git add / commit (x times)

git log / lg

git checkout master

git merge <branchname>

git push

git branch -d <branchname>
```
