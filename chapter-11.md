## Chapter 11 - Branch based development: 
* Protected branches
 * Why do we protect branches? which ones to protect? against what?
* Short lived / long lived branches
* Various git work flows
 * Simple master branch based workflow
 * PR/MR based workflow (both private and public projects)
 * Git Flow (Horrible. Stay away)
 * Git Phlow by Praqma (completely automated pipelines)
